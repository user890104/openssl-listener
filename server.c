#include <unistd.h>

#ifdef _WIN32
#include <ws2tcpip.h>
#include "win32_fork.h"
#else
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#endif

#include "openssl/ssl.h"
#include "openssl/err.h"

#include "tcp.h"
#include "ssl.h"
#include "client_handler.h"

#ifndef _WIN32
void sig_handler(int signum) {
    if (signum == SIGCHLD) {
        wait(NULL);
        fprintf(stderr, "child process died\n");
    }
}
#endif

int main(int count, char *strings[]) {
#ifdef _WIN32
    WSADATA wsaData;
#endif

    SSL_CTX *ctx;
    int server;
    char *portnum;
    char listening = 1;

    if (count != 2) {
        printf("Usage: %s <portnum>\n", strings[0]);
        return EXIT_FAILURE;
    }

#ifdef _WIN32
    if (WSAStartup(MAKEWORD(1,1), &wsaData) == SOCKET_ERROR) {
        printf("Error initialising WSA.\n");
        return EXIT_FAILURE;
    }
#else
    signal(SIGCHLD, sig_handler);
#endif

    SSL_library_init();

    portnum = strings[1];
    ctx = InitServerCTX(); // initialize SSL
    LoadCertificates(ctx, "server.pem", "server.key"); // load certs
    server = OpenListener(atoi(portnum)); // create server socket

    while (listening) {
        struct sockaddr_in addr;
        socklen_t len = sizeof(addr);

        int client = accept(server, (struct sockaddr*)&addr, &len); // accept connection as usual
        
        if (client < 0) {
            perror("accept");
            continue;
        }
        
        int pid = fork();
        
        switch (pid) {
            case -1:
                perror("fork");
                close(client);
                close(server);
                SSL_CTX_free(ctx);
                return EXIT_FAILURE;
            break;
            case 0:
                // child process
                fprintf(stderr, "child process created\n");
                listening = 0;
                close(server);
                server = 0;
                printf("Connection open: %s:%d\n", inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
                handle_client(client, ctx); // service connection
                printf("Connection closed: %s:%d\n", inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
                close(client);
            break;
            default:
                // parent process
                close(client);
            break;
        }
    }

    if (server) {
        close(server); // close server socket
    }
    
    SSL_CTX_free(ctx); // release context
    
    return EXIT_SUCCESS;
}
