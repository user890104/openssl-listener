TARGET=server
CC?=gcc
CFLAGS=-Wall

SOURCES=server.c ssl.c tcp.c client_handler.c

UNAME := $(shell uname)
LOADLIBES=-lssl -lcrypto

ifeq ($(OS),Windows_NT)
CFLAGS+=-I/d/Work/hook/libs/openssl/include
LDFLAGS+=-L/d/Work/hook/libs/openssl/lib
LOADLIBES+=-lws2_32 -lgdi32
SOURCES+=win32_fork.c
endif

OBJECTS=$(SOURCES:.c=.o)

all: $(SOURCES) $(TARGET)

$(TARGET): $(OBJECTS)

clean:
	rm -rf *.o *.exe $(TARGET)
