#include <unistd.h>

#include "openssl/ssl.h"
#include "openssl/err.h"

#include "ssl.h"

SSL_CTX* InitServerCTX(void) {
    const SSL_METHOD *method;
    SSL_CTX *ctx;

    OpenSSL_add_all_algorithms();  /* load & register all cryptos, etc. */
    SSL_load_error_strings();   /* load all error messages */
    method = SSLv23_server_method();  /* create new server-method instance */
    ctx = SSL_CTX_new(method);   /* create new context from method */

    if (ctx == NULL) {
        ERR_print_errors_fp(stderr);
        exit(EXIT_FAILURE);
    }

    return ctx;
}

void LoadCertificates(SSL_CTX* ctx, char* CertFile, char* KeyFile) {
    /* set the local certificate from CertFile */
    if (SSL_CTX_use_certificate_file(ctx, CertFile, SSL_FILETYPE_PEM) <= 0) {
        ERR_print_errors_fp(stderr);
        exit(EXIT_FAILURE);
    }

    /* set the private key from KeyFile (may be the same as CertFile) */
    if (SSL_CTX_use_PrivateKey_file(ctx, KeyFile, SSL_FILETYPE_PEM) <= 0) {
        ERR_print_errors_fp(stderr);
        exit(EXIT_FAILURE);
    }

    /* verify private key */
    if (!SSL_CTX_check_private_key(ctx)) {
        fprintf(stderr, "Private key does not match the public certificate\n");
        exit(EXIT_FAILURE);
    }
}

void ShowCerts(SSL* ssl) {
    X509 *cert;
    char *line;

    cert = SSL_get_peer_certificate(ssl); /* Get certificates (if available) */

    if (cert != NULL) {
        printf("Server certificates:\n");
        line = X509_NAME_oneline(X509_get_subject_name(cert), 0, 0);
        printf("Subject: %s\n", line);
        free(line);
        line = X509_NAME_oneline(X509_get_issuer_name(cert), 0, 0);
        printf("Issuer: %s\n", line);
        free(line);
        X509_free(cert);
    }
    else {
        printf("No certificates.\n");
    }
}

char *sslRead(SSL *ssl) {
    const int readSize = 1024;
    char *result = NULL;
    int received, count = 0;
    char buffer[1024];

    result = malloc(readSize * sizeof(char) + 1);
    
    if (!result) {
        exit(EXIT_FAILURE);
    }
    
    result[0] = '\0';
    
    if (ssl) {
        while (1) {
            result = realloc(result, (count + 1) * readSize * sizeof(char) + 1);
            
            if (!result) {
                exit(EXIT_FAILURE);
            }

            buffer[0] = '\0';
            received = SSL_read(ssl, buffer, readSize);

            if (received > 0) {
                buffer[received] = '\0';
                strcat(result, buffer);
            }
            
            if (received < readSize || SSL_pending(ssl) == 0) {
                break;
            }
            
            count++;
        }
    }

    return result;
}
