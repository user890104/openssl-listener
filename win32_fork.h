#ifndef __WIN32_FORK__
#define __WIN32_FORK__

#include <unistd.h>

pid_t fork(void);

#endif