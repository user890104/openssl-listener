#ifndef __SSL_H__
#define __SSL_H__

#define FAIL -1

SSL_CTX* InitServerCTX(void);
void LoadCertificates(SSL_CTX* ctx, char* CertFile, char* KeyFile);
void ShowCerts(SSL* ssl);
char *sslRead(SSL *ssl);

#endif
