#include "openssl/ssl.h"
#include "openssl/err.h"

#include "ssl.h"
#include "client_handler.h"

// Serve the connection
void handle_client(int client, SSL_CTX *ctx) {
    char *request = NULL;
    size_t len;
    char *response = "HTTP/1.0 405 Method Not Allowed\r\nContent-Length: 0\r\n\r\n";
    SSL *ssl;

    ssl = SSL_new(ctx); //get new SSL state with context
    SSL_set_fd(ssl, client); //set connection socket to SSL state

    // do SSL-protocol accept
    if (SSL_accept(ssl) == FAIL) {
        ERR_print_errors_fp(stderr);
        SSL_free(ssl);
        return;
    }
    
    ShowCerts(ssl); // get any certificates
    
    do {
        if (request) {
            free(request);
            request = NULL;
        }
    
        request = sslRead(ssl); // get request
        len = strlen(request);
        
        if (len > 0) {
#ifdef _WIN32
            printf("Echo(%u): %s\n", len, request);
#else
            printf("Echo(%zu): %s\n", len, request);
#endif
            
            printf("send\n");
            SSL_write(ssl, response, strlen(response)); // send request back
        }
        else {
            ERR_print_errors_fp(stderr);
            break;
        }
    }
    while (strlen(request) > 0);
    
    if (request) {
        free(request);
        request = NULL;
    }

    SSL_free(ssl); //release SSL state
}
